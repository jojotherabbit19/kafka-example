using Producer.API.Services;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

#region AppSettings

var kafkaConfig = builder.Configuration.GetSection("KafkaConfig").Get<KafkaConfigModel>();
builder.Services.AddSingleton(kafkaConfig);

// add services
// best practices: using DI containe
builder.Services.AddSingleton<ITransactionServices, TransactionServices>();


#endregion

#region App
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

#endregion