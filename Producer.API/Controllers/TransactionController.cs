using Microsoft.AspNetCore.Mvc;
using Producer.API.Services;

namespace Producer.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TransactionController : ControllerBase
    {
        private readonly ITransactionServices _transactionServices;

        public TransactionController(ITransactionServices transactionServices)
        {
            _transactionServices = transactionServices;
        }

        [HttpPost]
        public async Task<IActionResult> NewTransaction([FromBody] TransactionRecord record)
        {
            var rs = await _transactionServices.NewTransaction(record);
            // temp condition
            // best paractices rs should be as a response model
            // return Ok(rs)
            return Ok(rs != null ? "success" : "fail");
        }
    }
}