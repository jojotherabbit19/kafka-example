﻿namespace Producer.API.Services
{
    public interface ITransactionServices
    {
        Task<TransactionRecord> NewTransaction(TransactionRecord transactionRecord);
    }
}
