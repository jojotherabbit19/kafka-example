﻿using Confluent.Kafka;
using Newtonsoft.Json;
using System.Text;

namespace Producer.API.Services
{
    public class TransactionServices : ITransactionServices
    {
        private readonly KafkaConfigModel _kafkaConfig;
        private readonly IProducer<string, TransactionRecord> _producer;
        public TransactionServices(KafkaConfigModel kafkaConfig)
        {
            _kafkaConfig = kafkaConfig;
            var config = new ProducerConfig
            {
                BootstrapServers = _kafkaConfig.BootstrapServers,
                AllowAutoCreateTopics = true,
            };
            _producer = new ProducerBuilder<string, TransactionRecord>(config)
                                .SetValueSerializer(new TransactionRecord())
                                .Build();
        }
        public Task<TransactionRecord> NewTransaction(TransactionRecord transactionRecord)
        {
            //todo

            // key should depend on use (should use TransactionRecord Id as Key ?)
            var key = Guid.NewGuid().ToString();
            var msg = new Message<string, TransactionRecord>
            {
                Key = key,
                Value = transactionRecord
            };
            _producer.Produce(_kafkaConfig.Topic, msg);

            return Task.FromResult(transactionRecord);
        }
    }

    #region Model
    public class TransactionRecord : ISerializer<TransactionRecord>
    {
        public decimal TransactionAmount { get; set; }
        public DateTime TransactionDate { get; set; }
        public Guid From { get; set; }
        public Guid To { get; set; }
        public byte[] Serialize(TransactionRecord data, SerializationContext context)
        {
            if (data == null)
            {
                return null;
            }
            var jsonString = JsonConvert.SerializeObject(data);
            return Encoding.UTF8.GetBytes(jsonString);
        }
    }
    public class KafkaConfigModel
    {
        public string BootstrapServers { get; set; }
        public string Topic { get; set; }
        public string TransactionGroup { get; set; }
    }
    #endregion
}
