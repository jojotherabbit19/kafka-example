using Consumer.Worker;

IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices((hostContext, services) =>
    {
        var kafkaConfig = hostContext.Configuration.GetSection("KafkaConfig").Get<KafkaConfigModel>();
        services.AddSingleton(kafkaConfig);

        services.AddHostedService<TransactionConsumer>();
    })
    .Build();

host.Run();
