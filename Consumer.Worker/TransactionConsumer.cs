using Confluent.Kafka;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Text;

namespace Consumer.Worker
{
    public class TransactionConsumer : BackgroundService
    {
        private readonly ILogger<TransactionConsumer> _logger;
        private readonly KafkaConfigModel _kafkaConfig;
        private readonly IConsumer<string, TransactionRecord> _consumer;
        public TransactionConsumer(ILogger<TransactionConsumer> logger,
            KafkaConfigModel kafkaConfig)
        {
            _logger = logger;
            _kafkaConfig = kafkaConfig;
            var config = new ConsumerConfig
            {
                AutoOffsetReset = AutoOffsetReset.Earliest,
                BootstrapServers = kafkaConfig.BootstrapServers,
                GroupId = kafkaConfig.TransactionGroup,
                AllowAutoCreateTopics = true
            };

            _consumer = new ConsumerBuilder<string, TransactionRecord>(config)
                            .SetValueDeserializer(new TransactionRecord())
                            .Build();

            _consumer.Subscribe(_kafkaConfig.Topic);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                var consumeResult = _consumer.Consume(stoppingToken);
                // convert obj to json => log
                var obj = JsonConvert.SerializeObject(consumeResult.Value);
                _logger.LogInformation("Consume message at: {time}\nMessage: {transaction}", DateTimeOffset.Now, obj);
                await Task.Delay(1000, stoppingToken);

                //todo
            }
        }
    }
    #region Model
    public class TransactionRecord : IDeserializer<TransactionRecord>
    {
        public decimal TransactionAmount { get; set; }
        public DateTime TransactionDate { get; set; }
        public Guid From { get; set; }
        public Guid To { get; set; }
        public TransactionRecord Deserialize(ReadOnlySpan<byte> data, bool isNull, SerializationContext context)
        {
            if (isNull)
            {
                return null; // Handle null values if necessary
            }
            try
            {
                // Convert the byte array to a UTF-8 encoded string
                string jsonString = Encoding.UTF8.GetString(data);
                // Deserialize the JSON string into a TransactionRecord object
                return JsonConvert.DeserializeObject<TransactionRecord>(jsonString);
            }
            catch (Exception ex)
            {

                throw new SerializationException("Error deserializing TransactionRecord.", ex);
            }
        }
    }
    public class KafkaConfigModel
    {
        public string BootstrapServers { get; set; }
        public string Topic { get; set; }
        public string TransactionGroup { get; set; }
    }
    #endregion
}